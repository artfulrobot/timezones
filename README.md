# Timezones app

I wanted to create a simple ad-free privacy aware app to show timezones, e.g. for a meeting.

Try it out at https://artfulrobot.uk/lil/timezones

All data is stored in the URL's fragment, so when sending the link around the webserver won't ever see the data.
