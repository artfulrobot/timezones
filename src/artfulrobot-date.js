Date.newDateFromISOAndTimeZone = function newDateFromISOAndTimeZone(datetimeStringWithoutOffset, timeZone) {
  let d = new Date(datetimeStringWithoutOffset + 'Z');
  if (d.toString() === 'Invalid Date') {
    console.warn("Invalid date: ", JSON.stringify(datetimeStringWithoutOffset));
  }
  let offset = d.getUTCOffsetString(timeZone)
  let d2 = new Date(datetimeStringWithoutOffset + offset);
  let d2iso = d2.toISOStringForTimeZone(timeZone);
  if (d2iso !== datetimeStringWithoutOffset.substr(0, d2iso.length)) {
    let d2utc = new Date(d2iso + 'Z');
    let minsDiff = (d2utc.getTime() - d.getTime())/60000;
    d2.setMinutes(d2.getMinutes() - minsDiff);
  }
  // The input should match the output.
  return d2; 
}

Date.prototype.toISOStringForTimeZone = function toISOStringForTimeZone(timeZone, includeOffset) {
  if (!timeZone) {
    timeZone = Date.getBrowserTimeZone();
  }
  // en-GB style for this is dd/mm/yyyy, hh:mm:ss
  let enGBFormat = this.toLocaleString('en-GB', {dateStyle: 'short', timeStyle:'medium', timeZone})
    .match(/(\d\d)\/(\d\d)\/(\d{4}), ([0-9:]{8})$/);
  if (!enGBFormat) {
    console.warn("enGBFormat failed to parse", this.toLocaleString('en-GB', {dateStyle: 'short', timeStyle:'medium', timeZone}), this);
  }
  let r = `${enGBFormat[3]}-${enGBFormat[2]}-${enGBFormat[1]}T${enGBFormat[4]}`;
  if (includeOffset) {
    r += this.getUTCOffsetString(timeZone)
  }
  return r;
}

Date.prototype.getUTCOffsetString = function getUTCOffsetString(timeZone) {
  let offset = this.toLocaleString('en-US',
    { timeZone, hour: 'numeric', hour12: false, minute: 'numeric', timeZoneName: 'longOffset'})
    .replace(/^.*?GMT/, '');

  return offset || '+00:00';
}

Date.getBrowserTimeZone = function getBrowserTimeZone() {
  return Intl.DateTimeFormat().resolvedOptions().timeZone;
};

