import { writable } from 'svelte/store';

export const specified = writable({
  hours: [],
  date: new Date(),
  zone: Intl.DateTimeFormat().resolvedOptions().timeZone,
});


